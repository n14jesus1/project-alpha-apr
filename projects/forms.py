from django.forms import ModelForm
from projects.models import Project, Company


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
            "company",
        ]


class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = [
            "name",
        ]
