from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Company(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )
    company = models.ForeignKey(
        Company, related_name="projects", on_delete=models.PROTECT, null=True
    )

    def __str__(self):
        return self.name
