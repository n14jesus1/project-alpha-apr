from django.urls import path
from projects.views import (
    project_list,
    show_project,
    create_project,
    company_detail,
    company_search,
)


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path(r"companies/", company_search, name="companies"),
    path("companies/<int:id>", company_detail, name="company_detail"),
]
