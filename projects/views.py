from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from projects.models import Project, Company
from projects.forms import ProjectForm


# Create your views here.
@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/project_list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project_object": project,
    }
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)


@login_required
def company_search(request):
    all_companies = Company.objects.all()
    if request.method == "GET":
        query = request.GET.get("q")
        submitbutton = request.GET.get("submit")
        if query is not None:
            lookups = Q(name__icontains=query)
            companies = Company.objects.filter(lookups).distinct()
            context = {
                "companies": companies,
                "submitbutton": submitbutton,
            }
            return render(request, "projects/company_search.html", context)
        else:
            context = {
                "all_companies": all_companies,
            }
        return render(request, "projects/company_search.html", context)
    else:
        context = {
            "all_companies": all_companies,
        }
        return render(request, "projects/company_search.html", context)


@login_required
def company_detail(request, id):
    company = get_object_or_404(Company, id=id)
    projects = Project.objects.filter(company=company)
    context = {
        "company": company,
        "projects": projects,
    }
    return render(request, "projects/company_detail.html", context)
