from django.forms import ModelForm
from django import forms
from tasks.models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
            "notes",
        ]
        widgets = {
            "start_date": forms.TextInput(
                {
                    "placeholder": "YYYY-MM-DD hh:mm:ss",
                    "type": "datetime-local",
                },
            ),
            "due_date": forms.TextInput(
                {
                    "placeholder": "YYYY-MM-DD hh:mm:ss",
                    "type": "datetime-local",
                }
            ),
        }


class EditTaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "is_completed",
            "project",
            "assignee",
            "notes",
        ]
        widgets = {
            "start_date": forms.TextInput(
                {
                    "placeholder": "YYYY-MM-DD hh:mm:ss",
                    "type": "datetime-local",
                },
            ),
            "due_date": forms.TextInput(
                {
                    "placeholder": "YYYY-MM-DD hh:mm:ss",
                    "type": "datetime-local",
                }
            ),
        }
